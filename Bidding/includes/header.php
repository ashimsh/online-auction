<!DOCTYPE HTML>
<!-- Website Template by freewebsitetemplates.com -->
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $title; ?></title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/components/form.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/components/form.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/components/icon.min.css">

<link rel="stylesheet"  href="./css/button.min.css">
<style>
#navv { display: none; }
</style>

        <link rel="stylesheet" href="css/style.css" type="text/css">
    </head>
    <body>

        <div id="header">
            <a href="index.php" class="logo"><img src="images/logo.png" alt="Home Page"></a>
            <ul>

                <li></li>
                <li <?php echo $title == "Diagnostic Center" ? 'class="selected"' : ''; ?> >
                    <a href="index.php">home</a>
                </li>

                <li <?php echo $title == "about" ? 'class="selected"' : ''; ?>>
                    <a href="about.php">about</a>
                </li>

                <li <?php echo $title == "service" ? 'class="selected"' : ''; ?> >
                    <a href="services.php">services</a>
                </li>

                <li <?php echo $title == "New Appoinment" ? 'class="selected"' : ''; ?>>
                    <a href="appoinment.php">New Appoinemnt</a>
                </li>

                <li><a href="#" id="login">Login</a>
                    <div id="navv">
                        <a href="http://localhost/patient/admin/"><i class="spy icon"></i>&nbsp;Admin</a>
                        <a href="http://localhost/patient/doctor/"><i class="doctor icon"></i>Doctor</a>
                        <a href="http://localhost/patient/patient/"><i class="user icon"></i>&nbsp;Patient</a>
                    </div>
                </li>

            </ul>
        </div>

<script type="text/javascript">
var i = 0;
// console.log(i);
    document.getElementById('login').onclick = function(){
        if(i == 0){
            document.getElementById("navv").style.display="block";
            i = 2;
            return;
            //  console.log(i);
        }
        if(i == 2)
            document.getElementById('navv').style.display = "none";
            i = 0;
            return;
            // console.log(i);
    }
</script>