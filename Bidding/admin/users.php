<?php
include "includes/session.php";
include "includes/dbconfig.php"; 

$type = "users";
$tablename = $type;

if (isset($_POST['create'])) {
    $fullname = $_POST['fullname'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    $email = $_POST['email'];
    $status = $_POST['status'];

    $sql = "INSERT INTO $tablename(fullname, username, password, email, status) VALUES('$fullname', '$username', '$password', '$email', '$status')";
    $query = mysqli_query($con, $sql);
    if ($query) {
        $success = "User insert sucessfully";
    } else {
        $error = "User not inserted. Mysql says:". mysqli_error($con);
    }
}

if (isset($_POST['save'])) {
    $fullname = $_POST['fullname'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    $email = $_POST['email'];
    $status = $_POST['status'];
    $id = $_POST['edit'];

    $sql = "UPDATE $tablename SET fullname = '$fullname', username='$username', password='$password', email='$email', status='$status'  WHERE id='$id'";
    $query = mysqli_query($con, $sql);
    if ($query) {
        $success = "User insert sucessfully";
    } else {
        $error = "User not inserted. Mysql says:". mysqli_error($con);
    }
}

if (isset($_GET['del'])) {
    $id = $_GET['del'];

    $sql = "DELETE FROM $tablename WHERE id = '$id'";
    $query = mysqli_query($con, $sql);

    if ($query) {
        $msg = "User insert sucessfully";
    } else {
        $msg = "User not inserted";
    }
}

if (isset($_GET['edit'])) {
    $id = $_GET['edit'];

    $sql = "SELECT * FROM $tablename WHERE id = '$id'";
    $query = mysqli_query($con, $sql);
    $editUser = mysqli_fetch_array($query);
}

if (isset($_GET['view'])) {
    $id = $_GET['view'];

    $sql = "SELECT * FROM $tablename WHERE id = '$id'";
    $query = mysqli_query($con, $sql);
    $viewUser = mysqli_fetch_array($query);
}

if (isset($_POST['search'])) {
    $searchby = $_POST['searchby'];
    $searchkey = $_POST['searchkey'];

    $sql = "SELECT * FROM $tablename WHERE $searchby LIKE '%$searchkey%'";
   // echo $sql;
} else {
    //get all record
    $sql = "SELECT * FROM $tablename";
}

$query = mysqli_query($con, $sql);
$count = mysqli_num_rows($query);



include "includes/header.php";
?>

<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?php echo ucfirst($type); ?> Management</h1>
            </div>
            <?php
            include "includes/links.php";
            include "includes/message.php";
            ?>
            <div class="col-lg-12">
                <?php
                if (isset($_GET['new']) || isset($_GET['edit'])) {
                    include "manager/$type/form.php";
                }elseif(isset($_GET['view'])){
                   include "manager/$type/view.php"; 
                } 
                else {
                    include "manager/$type/search.php";
                    include "manager/$type/datalist.php";
                }
                ?>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
<?php
include "includes/footer.php";
?>