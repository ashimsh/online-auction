-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 03, 2018 at 03:56 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `onlineauction`
--

-- --------------------------------------------------------

--
-- Table structure for table `bid`
--

CREATE TABLE `bid` (
  `id` int(11) NOT NULL,
  `pimage` varchar(255) NOT NULL,
  `type` varchar(200) NOT NULL,
  `pname` varchar(70) NOT NULL,
  `minbid` int(50) NOT NULL,
  `bidendtime` varchar(255) NOT NULL,
  `approved` int(12) NOT NULL,
  `newprice` int(20) NOT NULL,
  `datee` date NOT NULL,
  `username` varchar(222) NOT NULL,
  `win_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bid`
--

INSERT INTO `bid` (`id`, `pimage`, `type`, `pname`, `minbid`, `bidendtime`, `approved`, `newprice`, `datee`, `username`, `win_status`) VALUES
(20, 'product_image/fb32eb7fd2e2164a83808dd89c463627car2.jpg', '1', 'Hyundai i20', 18800, '1543844700', 1, 70000, '2018-12-03', 'root', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bidhistory`
--

CREATE TABLE `bidhistory` (
  `id` int(11) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `productid` varchar(50) NOT NULL,
  `bidamt` varchar(50) NOT NULL,
  `bidtime` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bidhistory`
--

INSERT INTO `bidhistory` (`id`, `user_id`, `productid`, `bidamt`, `bidtime`) VALUES
(1, '15', '20', '60000', '2018-12-03'),
(2, '16', '20', '70000', '2018-12-03');

-- --------------------------------------------------------

--
-- Table structure for table `cat`
--

CREATE TABLE `cat` (
  `id` int(20) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cat`
--

INSERT INTO `cat` (`id`, `type`) VALUES
(1, 'Car'),
(2, 'Bike'),
(3, 'Watch'),
(4, 'Jwellery');

-- --------------------------------------------------------

--
-- Table structure for table `newbid`
--

CREATE TABLE `newbid` (
  `id` int(11) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `productid` varchar(50) NOT NULL,
  `bidamt` varchar(50) NOT NULL,
  `bidtime` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newbid`
--

INSERT INTO `newbid` (`id`, `user_id`, `productid`, `bidamt`, `bidtime`) VALUES
(21, '17', ' 20', '6000', '2018-11-14  09:14:48');

-- --------------------------------------------------------

--
-- Table structure for table `recommendation`
--

CREATE TABLE `recommendation` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `pcat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recommendation`
--

INSERT INTO `recommendation` (`id`, `user_id`, `pid`, `pcat`) VALUES
(14, 15, 20, 0),
(15, 15, 22, 0),
(16, 15, 22, 0),
(17, 15, 23, 0),
(18, 15, 23, 0),
(19, 15, 23, 0),
(20, 15, 20, 0),
(21, 15, 20, 0),
(22, 15, 20, 0),
(23, 0, 20, 0),
(24, 0, 22, 0),
(25, 0, 22, 0),
(26, 0, 24, 0),
(27, 15, 20, 0),
(28, 15, 20, 0),
(29, 15, 20, 0),
(30, 15, 20, 0),
(31, 15, 20, 0),
(32, 15, 20, 0),
(33, 15, 20, 0),
(34, 15, 20, 0),
(35, 0, 22, 0),
(36, 16, 22, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL,
  `citizenship_no` varchar(255) NOT NULL,
  `pimage` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `address` varchar(225) NOT NULL,
  `contact_no` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `usertype` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `citizenship_no`, `pimage`, `firstname`, `lastname`, `address`, `contact_no`, `email_address`, `username`, `password`, `usertype`) VALUES
(14, '1234', 'product_image/3b4864524f62330151162d932c71e5a7startform.PNG', 'admin', 'admin', 'kapan', '973465', 'admin@localhost.com', 'admin', 'admin', 1),
(15, '123', 'product_image/b8967c55e1701f52d8b70dbdda3a6155startform.PNG', 'deepa', 'sharma', 'kapan', '973465', 'deepasharma@gmail.com', 'deepa', 'sharma', 0),
(16, '89878979798779798798', 'product_image/7263bcdcbcf04fa96a2e1ad00290dfa8images (1).jfif', 'tutuytyutyguyg', 'ghjgjhghjg', 'jhkjhkjhj', '98989898797897', 'kkjhjkhjkh@hkhjkhkjh.com', 'amit2', 'lol123', 0),
(17, 'Duis natus ullam ut ut qui dignissimos incididunt', 'product_image/bc85e8d3a9675b3b03c0a56a2c0f9f91one.png', 'Taylor', 'Vasquez', 'Est id natus nihil maiores qui sint mollit neque qui quaerat nulla a fugiat saepe est nostrud', '+653-18-1435061', 'sevin@yahoo.com', 'qaqojuc', 'sadasdas', 0);

-- --------------------------------------------------------

--
-- Table structure for table `winner`
--

CREATE TABLE `winner` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `winner`
--

INSERT INTO `winner` (`id`, `user_id`, `pid`, `amount`) VALUES
(23, 16, 20, 70000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bid`
--
ALTER TABLE `bid`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bidhistory`
--
ALTER TABLE `bidhistory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cat`
--
ALTER TABLE `cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newbid`
--
ALTER TABLE `newbid`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recommendation`
--
ALTER TABLE `recommendation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `winner`
--
ALTER TABLE `winner`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bid`
--
ALTER TABLE `bid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `bidhistory`
--
ALTER TABLE `bidhistory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cat`
--
ALTER TABLE `cat`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `newbid`
--
ALTER TABLE `newbid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `recommendation`
--
ALTER TABLE `recommendation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `winner`
--
ALTER TABLE `winner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
