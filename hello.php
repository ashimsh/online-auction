<center>
<table width="530">
<tbody><tr>
	<td width="50%" align="left">
	
		<p><a href="/action/faqSearch">Help Menu / Ask a Question</a><br>
		Read up on specific topics. Send an inquiry to Customer Support.</p>
		
		<p><a href="/action/howTo">How to Bid</a><br>
		A quick outline of how to bid, win, and book your travel with us.</p>
		
		<p><a href="/action/emailPassword">Forgot Your Password?</a><br>
		Click here to retrieve your password, handle, and customer ID.</p>
		
		<p><a href="/action/travelerInfo">Traveler Information</a><br>
		Links to the latest passport, vaccine, health update, weather, and airport 
		security information.</p>
		
	</td>
	
	<td width="50%" align="left">
		<!--<p><a href="/cust_care/tutorial/"> Tutorial</a><br>
		Review our simple, step-by-step SkyAuction user tutorial.</p>-->
		
		<p><a href="/members/home.do">Your Account</a><br>
		View your Member information and won auctions, review your travel itinerary 
		or change your account settings.</p>
		
		<p><a href="/mailList.sky">Email Alerts</a><br>
		Click here to sign up for newsletters.</p>
		
		<p><a href="/tac.do">Terms of Use</a><br>
		Read about SkyAuction's policies before you place your bid.</p>
		
		</td>
	
</tr>
</tbody></table>
</center>