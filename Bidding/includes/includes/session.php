<?php

//starting session
session_start();

if (isset($_COOKIE['mycookie'])) {
    $_SESSION['docLogin'] = $_COOKIE['mycookie'];
}

if (!isset($_SESSION['docLogin'])) {
    header("location: login.php");
}elseif(!isset($_SESSION['adminLogin'])){
    header("location: login.php");   
}elseif(!isset($_SESSION['patientLogin'])){
    header("location: login.php");
}
