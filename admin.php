<?php
  session_start();
  if ($_SESSION['status'] != 'logedin') {
    header('location:login.php');
  }
?>
<?php
  include "header.php";
?>
<?php
  include "header1.php";
?>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <?php
          if(isset($_SESSION['status'])){                      
            if($_SESSION['status']=='logedin'){
        ?>
              <li class="dropdown">
                <a href="#" class="dropdown" data-toggle="dropdown" role="button" aria-expanded="false">
                    <span class=" "></span>Welcome <?php echo $_SESSION['username']; ?> <span class="caret"></span>
                </a>                                      
                <ul role="menu" class="sub-menu">
                    <li><a href="logout.php"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>        
                </ul>
              </li>
            <?php } 
          }else{ ?>
            <li><a href="login.php" >Login</a></li>
          <?php } ?>
          <li><a href="productrequest.php" >Product Requested</a></li>
          <li><a href="category.php">Add Category</a></li>
          <li><a href="user.php">View users</a></li>
          <li><a href="winner.php">Winner</a></li>

    <!-- <li><a href="adminedit.php">Edit Profile</a></li> -->
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous">
</script>
<script 
  src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" 
  integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" 
  crossorigin="anonymous">
</script>
