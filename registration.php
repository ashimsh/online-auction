<?php
include "header.php";
?>

<?php
include "header3.php"
?>

<!DOCTYPE html>
<html>
<head> 
    <title>Registration Form</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.validate.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $().ready(function() {
            $("#form1").validate({
                rules: {
                    "citizenship_no": "required",
                    "email":{
                        required:true,
                        email:true
                    },
                    "firstname": "required",
                    "lasttname": "required",
                    "password": "required",
                    "cnfpassword": {
                        required:true,
                        equalTo: "#password"
                    }
                },
                messages: {
                    "citizenship_no": "<p style='color:red'>This Field is required</p>",
                    "email": {
                        required:"<p style='color:red'>This Field is required</p>",
                        email:"<p style='color:red'>Please Enter valid email</p>"
                        },
                    "firstname": "<p style='color:red'>This Field is required</p>",
                    "lasttname": "<p style='color:red'>This Field is required</p>",
                    "password": "<p style='color:red'>This Field is required</p>",
                    "cnfpassword": {
                        required:"<p style='color:red'>This Field is required</p>",
                        equalTo: "<p style='color:red'>Password do not match</p>"
                    }
                }
            });   
        });
    </script>   

    <?php
        $link = mysqli_connect("localhost", "root", "");
        mysqli_select_db($link, "onlineauction");
        if (isset($_POST["submit1"])) {
            require_once 'dbconnection.php';
            $v1 = rand(1111, 9999);
            $v2 = rand(1111, 9999);
            $v3 = $v1 . $v2;
            $v3 = md5($v3);
            $fnm = $_FILES["pimage"]["name"];
            $dst = "./product_image/" . $v3 . $fnm;
            $dst1 = "product_image/" . $v3 . $fnm;
            move_uploaded_file($_FILES["pimage"]["tmp_name"], $dst); 
            if (mysqli_query($link, "insert into user values('','$_POST[citizenship_no]','$dst1','$_POST[firstname]'  , '$_POST[lastname]','$_POST[address]','$_POST[contact_no]','$_POST[email]','$_POST[username]','$_POST[password]',' ')") == true){
                echo"<script>alert('Register  successfully thankyou for registration with us ');</script>";
                echo"<script>window.location='index.php'</script>";
            }else{
                echo"<script>alert('couldn't register');</script>";
                echo"<script>window.location='registration.php'</script>";
            }
        }
    ?> 
</head>

<body>
    <h1>Please register </h1>
    <form id="form1" name="form1" action="" method="post" enctype="multipart/form-data">
        <table align="center" class="table table-responsive table-bordered" width="30%">
            <tr>
                <td width="40%">Citizenship Number</td>
                <td width="70%"><input type="text" required id="citizenship_no" name="citizenship_no" size="55" value="" required></td>
            </tr>
            <tr>
                <td width="40%">Citizenship image</td>
                <td><input type="file" required id="pimage" name="pimage" required></td>
            </tr>
            <tr>
                <td width="40%">First Name</td>
                <td><input type="text" required id="firstname" name="firstname" size="55"  value="" required></td>
            </tr>
            <tr>
                <td width="40%">Last Name</td>
                <td><input type="text" required id="lastname" name="lastname"   size="55"   value="" required></td>
            </tr>
            <tr>
                <td width="40%">Address</td>
                <td><input type="text" required id="address" name="address"   size="55"   value="" required></td>
            </tr>
            <tr>
                <td width="40%">Contact Number</td>
                <td><input type="tel" required id="contact_no" name="contact_no"   size="55"   value="" required></td>
            </tr>
            <tr>
                <td width="40%">Email</td>
                <td><input type="email" required id="email" name="email"   size="55"   value="" required></td>
            </tr>
            <tr>
                <td width="40%">Username</td>
                <td><input type="text" required id="username" name="username"   size="55"   value="" required></td>
            </tr>
            <tr>
                <td width="40%">Password</td>
                <td><input type="password" required id="password" name="password"   size="55"   value="" required></td>
            </tr>
            <tr>
                <td width="40%">Confirm Password</td>
                <td><input type="password" required id="cnfpassword" name="cnfpassword"   size="55"   value="" required></td>
            </tr>        
            <tr>
                <td colspan="2" align="center"><input type="submit" name="submit1" value="Submit" class="btn" style="left:0;"></td>
            </tr>
        </table>
    </form>         
</body>
</html>