<div class="panel-body">
    <?php
    if (isset($success)):
        ?>
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php
            echo $success;
            ?>
        </div>
        <?php
    elseif (isset($error)):
        ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php
            echo $error;
            ?>
        </div>
        <?php
    endif;
    ?>
</div>