<?php
    include"header.php";
?>
<?php
    include"header3.php";
?>
<?php
// session_start();
    if($_SESSION['status'] != 'logedin'){
        header('location:login.php');
    }
?>
<?php
    require_once 'dbconnection.php';
?>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
    function check_amount(){
        var newA =document.getElementById('newprice').value;
        var pid = document.getElementById('pid').value;
        var minbid = document.getElementById('minbid').value;
        if(newA){
            $.ajax({
                type:'POST',
                url:'ajaxAmount.php',
                data:{id:pid,newprice:newA},
                success:function(data){
                   if(data==1){ 
                       $('#bidpricemsg').html("<p id='emsg' style='color:red'>Please bid more than latest bid</p>");
                   }else{
                        if($('#emsg') !=null){
                            $('#emsg').remove();
                        }
                    }
                }
            }); 
        }
    }
</script>
<h2>Bid here</h2>
<?php
    include('recommendationByPid.php');
    $id=$_GET['id'];
    $query = "SELECT * FROM bid where id='$id'" ;
    $result = mysqli_query($connect, $query);
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_array($result)) {
?>
            <div class="col-sm-4">
                <form method="post"  action=" " >                
                    <div style="border: 1px solid #eaeaec; margin: -1px 19px 3px -1px; box-shadow: 0 1px 2px rgba(0,0,0,0.05); padding:10px;" align="center">
                        <img src="<?php echo $row["pimage"]; ?>" class="img-responsive" >
                        <h5 class="text-info">Product id: <?php echo $row["id"]; ?></h5>
                        <h5 class="text-info">Product name: <?php echo $row["pname"]; ?></h5>
                        <h5 class="text-info">Minimum biding price: Rs <?php echo $row["minbid"]; ?></h5>             
                        <h5 class="text-info">Biding endtime: <?php echo $row["bidendtime"]; ?></h5>
                        <h5 class="text-info">Latest bidding price: <?php echo $row["newprice"]; ?></h5>
                        <h5 class="text-info">Latest bidding date: <?php echo $row["datee"]; ?></h5>
                    </div>                           
                    <?php 
                        $id=$row["id"];
                        $pimage=$row["pimage"];
                        $pname = $row["pname"];
                        $minbid = $row["minbid"];
                        $bidendtime = $row["bidendtime"];
                        $newprice = $row["newprice"];
                        $datee = $row["datee"];
        }
    } 
                    ?> 
                </form>
            </div>
            <div class="col-md-6">
                <form action="newprice.php" method="post">
                    <div class="box">
                        <input type="hidden" id="pid" name="id" value="<?php echo $id; ?>">
                        <input type="hidden" name="pimage" value="<?php echo $pimage; ?>">
                        <input type="hidden" name="pname" value="<?php echo $pname; ?>">
                        <input type="hidden" id="minbid" name="minbid" value="<?php echo $minbid; ?>">
                        <input type="hidden" name="bidendtime" value="<?php echo $bidendtime; ?>">
                        <table border="1" class="table table-responsive table-bordered table-striped" width="100%">
                            <tr>
                                <td>Your bidding price</td>
                                <td><input type="number" name="newprice" id="newprice" required onblur="check_amount()">   
                                    <p  id="bidpricemsg"></p>
                                </td>
                            </tr>  
                            <tr>
                                <td><input type="hidden" name="datee" readonly   value="<?php echo date('Y-m-d'); ?>"></td>
                                <td colspan="2"><input type="submit" name="submit1" value="Bid"></td>
                            </tr>
                        </table>
                    </div>
                </form>
            </div>

